let fs = require('fs');

const get = model => {
    return fs.readFileSync(`db/${model}.json`);
};

const add = (res, model, item) => {
    let data = getArr(model);
    data.push(item);
    write(res, model, data);
};

const put = (res, model, item, param, paramName) => {
    let data = getArr(model);
    const index = data.findIndex(item => item[paramName].toString() === param);

    switch (model) {
        case 'user':
            if (item.name) data[index].name = item.name;
            if (item.records) data[index].records = item.records;
            if (item.greasiness) data[index].greasiness = item.greasiness;
            break;

        case 'settings':
            data[index].price = item.price;
            data[index].date = item.date;
            break;

        case 'fix':
            data[index].price = item.value;
            data[index].date = item.date;
            break;

    }

    write(res, model, data)

};

const del = (res, model, param) => {
    let data = getArr(model);
    const index = data.findIndex(item => item._id.toString() === param);

    data.splice(index, 1);

    write(res, model, data);
};

const sort = (res, model, data) => {
    write(res, model, data)
};


const getArr = model => {
    return JSON.parse(fs.readFileSync(`db/${model}.json`));

};

const write = (res, model, data) => {
    fs.writeFile(`db/${model}.json`, JSON.stringify(data),  'utf8', (err) => {
        if (err) throw err;
        return res.sendStatus(200)
    });
};

exports.get = get;
exports.add = add;
exports.put = put;
exports.del = del;
exports.sort = sort;