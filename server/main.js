let express = require('express');
const cors = require('cors');
let { get, add, put, del, sort } = require('./helper');
let bodyParser = require('body-parser');

let app = express();

const USER_MODEL = 'user';
const SETTING_MODEL = 'settings';
const FIX_MODEL = 'fix';

const clientUrl = 'http://127.0.0.1/:8080';
const corsOptions = {
    origin: clientUrl,
    optionsSuccessStatus: 200
};


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());
app.all('/*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    next();
});


app.get('/api', cors(corsOptions), (req, res) => {
    res.send('API is running');
});


//**************USER***********************************
app.get('/api/user', (req, res) => {

    const users = get(USER_MODEL);
    return res.send(users);
});

app.post('/api/user', (req, res) => {
    const user = {
        _id: Date.now(),
        name: req.body.name,
        records: [],
        greasiness: []
    };

    add(res, USER_MODEL, user);
});

app.put('/api/user/:id', (req, res) => {
    put(res, USER_MODEL, req.body, req.params.id, '_id');
});

app.delete('/api/user/:id', (req, res) => {
    del(res, USER_MODEL, req.params.id);
});

app.post('/api/user/sort', (req, res) => {
    sort(res, USER_MODEL, req.body.users)
});


//**************SETTING***********************************
app.get('/api/setting', (req, res) => {
    const st = get(SETTING_MODEL);
    return res.send(st);
});

app.post('/api/setting', (req, res) => {
    const setting = {
        _id: Date.now(),
        price: req.body.price,
        date: req.body.date
    };

    add(res, SETTING_MODEL, setting);
});

app.put('/api/setting/:date', (req, res) => {

    put(res, SETTING_MODEL, req.body, req.params.date, 'date');

});


//**************FIX***********************************
app.get('/api/fix', (req, res) => {
    const fix = get(FIX_MODEL);
    return res.send(fix);
});

app.post('/api/fix', (req, res) => {
    const fix = {
        _id: Date.now(),
        value: req.body.value,
        date: req.body.date
    };

    add(res, FIX_MODEL, fix);

});

app.put('/api/fix/:date', (req, res) => {

    put(res, FIX_MODEL, req.body, req.params.id, 'date');

});

app.listen(3000, () => {
    console.log('Start servers')
});
