let express = require('express');
const cors = require('cors');
let MongoClient = require('mongodb').MongoClient;
let ObjectId = require('mongodb').ObjectID;
let bodyParser = require('body-parser');
let app = express();

let db;
const clientUrl = 'http://127.0.0.1/:8080';
// const clientUrl = 'http://localhost:8081';
const corsOptions = {
    origin: clientUrl,
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};



app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());
app.all('/*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    next();
});


app.get('/api', cors(corsOptions), (req, res) => {
    res.send('API is running');
});

app.get('/api/user', (req, res) => {
    db.collection('users').find().toArray((err, docs) => {
        if (err) throw err;
        res.send(docs)
    })
});

app.post('/api/user', (req, res) => {
    const user = {
        name: req.body.name,
        lastName: req.body.lastName,
        records: [],
        greasiness: []
    };

    db.collection('users').insert(user, (err, result) => {
        if (err) throw err;
        return res.send(user)
    })
});

app.put('/api/user/:id', (req, res) => {
    const data = {};
    if (req.body.name) data.name = req.body.name;
    if (req.body.lastName) data.lastName = req.body.lastName;
    if (req.body.records) data.records = req.body.records;
    if (req.body.greasiness) data.greasiness = req.body.greasiness;

    db.collection('users').updateOne({_id: ObjectId(req.params.id)}, {$set: data}, (err, result) => {
        if (err) throw err;
        return res.sendStatus(200)
    });
});

app.delete('/api/user/:id', (req, res) => {
    db.collection('users').deleteOne({
        _id: ObjectId(req.params.id)
    }, (err, result) => {
        if (err) throw err;
        return res.sendStatus(200)
    })
});

app.get('/api/setting', (req, res) => {
    db.collection('settings').find().toArray((err, docs) => {
        if (err) throw err;
        res.send(docs)
    })
});

app.post('/api/setting', (req, res) => {
    const setting = {
        price: req.body.price,
        date: req.body.date
    };
    db.collection('settings').insert(setting, (err, result) => {
        if (err) throw err;
        return res.send(setting)
    })
});

app.put('/api/setting/:date', (req, res) => {
    const setting = {
        price: req.body.price,
        date: req.body.date
    };

    db.collection('settings').updateOne({date: req.params.date}, {$set: setting}, (err, result) => {
        if (err) throw err;
        return res.sendStatus(200)
    });
});

app.get('/api/fix', (req, res) => {
    db.collection('fix').find().toArray((err, docs) => {
        if (err) throw err;
        res.send(docs)
    })
});

app.post('/api/fix', (req, res) => {
    const fix = {
        value: req.body.value,
        date: req.body.date
    };
    db.collection('fix').insert(fix, (err, result) => {
        if (err) throw err;
        return res.send(fix)
    })
});

app.put('/api/fix/:date', (req, res) => {
    const fix = {
        value: req.body.value,
        date: req.body.date
    };

    db.collection('fix').updateOne({date: req.params.date}, {$set: fix}, (err, result) => {
        if (err) throw err;
        return res.sendStatus(200)
    });
});

MongoClient.connect('mongodb://localhost:27017', (err, client) => {
    if (err) throw err;
    db = client.db('ml');
    app.listen(3000, () => {
        console.log('Start servers')
    });
});
