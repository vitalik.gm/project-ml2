
let fs = require('fs');

const arrayData = JSON.parse(fs.readFileSync(`db/settings.json`));

const newArrayData = arrayData.map(item => {
    item.price = {
        firstPeriod: item.price,
        lastPeriod: 6
    };
    return item;
});

fs.writeFile(`db/settings.json`, JSON.stringify(newArrayData),  'utf8', (err) => {
        if (err) throw err;
});
